# lml2moodle

A script demonstrating the basic sync of LDAP users and classes from linuxmuster.net to a Moodle installation using its REST api.

FIRST OF ALL: THIS SCRIPT IS ALPHA, NOT READY FOR PRODUCTION, NOT EVEN FULLY TESTED

IT OFFERS QUITE BASIC FUNCTIONALITY AND IS MEANT AS A STARTING POINT FOR INDIVIDUAL SOLUTIONS.

I extracted this from a much bigger project (schulzeug), which offers a web based interface, many more options to sync groups/courses, nextcloud support, and much more. But this one is far from release.

As many people asked for q replacement for the OPENLML-ENROL-Plugin for moodle, this might be a good way to start.

It is so far working with a 6.2 linuxmuster-LDAP - for v7 someone has to check the ldap-fields.

I did not test "projects", but they should work as classes.

The course structure will be as this:

```
"Schüler"
  |-"5"
  | |-5a
  | |-5b
  |-"6"
  |-...
  |-Projektname
"Lehrer"
  |- "Lehrerzimmer"
```


Students are enrolled to their courses, teachers to the teachers room (as in linuxmuster there are no real teacher-course-relationships, schulzeug has a database for this).

